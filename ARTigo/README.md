
<!-- README.md is generated from README.Rmd. Please edit that file -->

# ARTigo

## Überblick

``` r
read_csv("artist.csv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   id = col_double(),
#>   name = col_character()
#> )
#> # A tibble: 6,973 x 2
#>            id name                                 
#>         <dbl> <chr>                                
#>  1 2010683207 Stanislaus Wyspiansky                
#>  2 2010683217 Charles Dufresnoy                    
#>  3 2010683227 Rembrandt Rijn                       
#>  4 2010683232 Peter Birmann                        
#>  5 2010683237 Johann Füssli                        
#>  6 2010683258 Alvise Vivarini                      
#>  7 2010683261 Giovanni Antonio Canal gen. Canaletto
#>  8 2010683264 Francesco Raibolini                  
#>  9 2010683269 Raffaellino del Garbo                
#> 10 2010683272 Giovanni Castiglione                 
#> # ... with 6,963 more rows

read_csv("resource.csv", col_types = NULL, guess_max = 10000)
#> Parsed with column specification:
#> cols(
#>   id = col_double(),
#>   artist_id = col_double(),
#>   title = col_character(),
#>   not_before = col_double(),
#>   not_after = col_double(),
#>   location = col_character(),
#>   institution = col_character(),
#>   path = col_character()
#> )
#> # A tibble: 54,497 x 8
#>       id artist_id title                   not_before not_after location    institution        path 
#>    <dbl>     <dbl> <chr>                        <dbl>     <dbl> <chr>       <chr>              <chr>
#>  1   130        NA Altes Rathaus                 1267      1267 Aachen      Rathaus            130.~
#>  2   136        NA Kirche Sankt Justinus ~        925       950 Höchst (Fr~ Kirche Sankt Just~ 136.~
#>  3   137        NA Kirche Sankt Justinus ~        925       950 Höchst (Fr~ Kirche Sankt Just~ 137.~
#>  4   138        NA Kirche Sankt Justinus ~        925       950 Höchst (Fr~ Kirche Sankt Just~ 138.~
#>  5   139        NA Cathédrale Saint-Lazar~       1132      1132 Autun       Cathédrale Saint-~ 139.~
#>  6   140        NA Maison Carrée                   -5        -4 Nîmes       Maison Carrée      140.~
#>  7   141        NA Basilica di San Pietro~       1506      1506 Roma        Basilica di San P~ 141.~
#>  8   219        NA Gesamtansicht                 1734      1739 Paris       Plan Turgot        219.~
#>  9   220        NA Marais                        1734      1739 Paris       Plan Turgot        220.~
#> 10   221        NA Temple                        1734      1739 Paris       Plan Turgot        221.~
#> # ... with 54,487 more rows

read_csv("tag.csv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   id = col_double(),
#>   name = col_character(),
#>   language = col_character()
#> )
#> # A tibble: 295,343 x 3
#>       id name    language
#>    <dbl> <chr>   <chr>   
#>  1     1 adler   de      
#>  2     2 akt     de      
#>  3     3 angriff de      
#>  4     4 baum    de      
#>  5     5 berg    de      
#>  6     6 blau    de      
#>  7     7 blätter de      
#>  8     8 dunkel  de      
#>  9     9 falke   de      
#> 10    10 feder   de      
#> # ... with 295,333 more rows

read_csv("tagging.csv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   id = col_double(),
#>   resource_id = col_double(),
#>   tag_id = col_double(),
#>   frequency = col_double()
#> )
#> # A tibble: 4,654,646 x 4
#>       id resource_id tag_id frequency
#>    <dbl>       <dbl>  <dbl>     <dbl>
#>  1     1           1     39         6
#>  2     2           1      3         3
#>  3     3           1     22         2
#>  4     4           1     30        13
#>  5     5           1      1        14
#>  6     6           1     24         5
#>  7     7           1     19         1
#>  8     8           1     26         1
#>  9     9           1     23         7
#> 10    10           1     43         6
#> # ... with 4,654,636 more rows

read_csv("relation.csv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   id = col_double(),
#>   resource_id_1 = col_double(),
#>   resource_id_2 = col_double(),
#>   type = col_character()
#> )
#> # A tibble: 8,706 x 4
#>       id resource_id_1 resource_id_2 type   
#>    <dbl>         <dbl>         <dbl> <chr>  
#>  1     1             1        338376 part of
#>  2     2          1017          2662 part of
#>  3     3         10588         10589 part of
#>  4     4         10589         10588 part of
#>  5     5         10929         10932 part of
#>  6     6         10932         10929 part of
#>  7     7          1096        314702 part of
#>  8     8         10983        313819 part of
#>  9     9         11225        313823 part of
#> 10    10         11521        344796 part of
#> # ... with 8,696 more rows
```

## Weitere Informationen

Für Bilder der Ressourcen siehe: Becker, Matthias; Bogner, Martin;
Bross, Fabian; Bry, François; Campanella, Caterina; Commare, Laura;
Cramerotti, Silvia; Jakob, Katharina; Josko, Martin; Kneißl, Fabian;
Kohle, Hubertus; Krefeld, Thomas; Levushkina, Elena; Lücke, Stephan;
Puglisi, Alessandra; Regner, Anke; Riepl, Christian; Schefels, Clemens;
Schemainda, Corina; Schmidt, Eva; Schneider, Stefanie; Schön, Gerhard;
Schulz, Klaus; Siglmüller, Franz; Steinmayr, Bartholomäus; Störkle,
Florian; Teske, Iris; Wieser, Christoph (2018): ARTigo – Social Image
Tagging \[Dataset and Images\], 15. November 2018, Open Data LMU, DOI:
<https://doi.org/10.5282/ubm/data.136>.
