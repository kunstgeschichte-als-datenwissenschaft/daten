
<!-- README.md is generated from README.Rmd. Please edit that file -->

# Iconclass

## Überblick

``` r
read_lines("metadata.ndjson", n_max = 1000) %>%
  map(
    ~ jsonlite::fromJSON(.x) %>% 
      map(~ paste(.x, collapse = "; ")) %>%
      tibble::as_tibble()
  ) %>%
  dplyr::bind_rows()
#> # A tibble: 1,000 x 8
#>    id     src      url         title                    artist    date  themes    collection        
#>    <chr>  <chr>    <chr>       <chr>                    <chr>     <chr> <chr>     <chr>             
#>  1 820a6~ "1\\820~ http://art~ Gefesselter Prometheus   Rubens, ~ 1611~ 91E461    Philadelphia Muse~
#>  2 0eebb~ "1\\0ee~ http://art~ Bonaparte franchissant ~ David, J~ 1801  46C131    Musée National    
#>  3 6f3f1~ "1\\6f3~ http://art~ Portrait du pape Pie VII David, J~ 1805  11H(...)~ Musée du Louvre   
#>  4 ba96c~ "1\\ba9~ http://art~ Venus und die Grazien e~ David, J~ 1824  92B4      Musées royaux des~
#>  5 9a973~ "1\\9a9~ http://art~ Portrait de Napoléon da~ David, J~ 1812  61B2(...) National Gallery ~
#>  6 7684b~ "1\\768~ http://art~ Léonidas aux Thermopyle~ David, J~ 1814  31A71; 9~ Musée du Louvre   
#>  7 51148~ "1\\511~ http://art~ Der Schwur der Armee na~ David, J~ 1808~ 61I(...)  Musée National du~
#>  8 e82f0~ "1\\e82~ http://art~ Portrait d'Antoine-Laur~ David, J~ 1788  49C       Metropolitan Muse~
#>  9 5965b~ "1\\596~ http://art~ Abteil dritter Klasse    Daumier,~ 1864  46C153    Metropolitan Muse~
#> 10 588c0~ "1\\588~ http://art~ Musik im Tuileriengarten Manet, É~ 1862  59A1      National Gallery  
#> # ... with 990 more rows
```

## Weitere Informationen

[Iconclass](http://iconclass.org/) kann mithilfe des folgenden
*Python*-Pakets durchsucht werden: <https://github.com/epoz/iconclass>.
Bilder zu den einzelnen Ressourcen sind über einen *IIIF*-Server
zugänglich:
<https://iiif.dhvlab.org/lmu/iart/src/full/full/0/default.jpg>. Statt
`src` ist der jeweilige Eintrag in der Spalte `src` einzufügen, z. B.
<https://iiif.dhvlab.org/lmu/iart/1/820a6d4f95.jpg/full/full/0/default.jpg>.
