
<!-- README.md is generated from README.Rmd. Please edit that file -->

# IMDb

## Überblick

``` r
read_tsv("title.connections.tsv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   tconst = col_character(),
#>   type = col_character(),
#>   value = col_character()
#> )
#> # A tibble: 835,043 x 3
#>    tconst    type    value           
#>    <chr>     <chr>   <chr>           
#>  1 tt4130520 follows /title/tt0324213
#>  2 tt4130520 follows /title/tt0362230
#>  3 tt4130520 follows /title/tt3135152
#>  4 tt4130520 follows /title/tt4130510
#>  5 tt0120742 follows /title/tt0097815
#>  6 tt0120742 follows /title/tt0110442
#>  7 tt0077775 follows /title/tt0072190
#>  8 tt0199444 follows /title/tt0409866
#>  9 tt0013959 follows /title/tt0182850
#> 10 tt0013959 follows /title/tt0010696
#> # ... with 835,033 more rows

read_tsv("title.keywords.tsv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   tconst = col_character(),
#>   keyword = col_character()
#> )
#> # A tibble: 3,993,878 x 2
#>    tconst    keyword            
#>    <chr>     <chr>              
#>  1 tt0060548 chopping off a hand
#>  2 tt0060548 hysterical female  
#>  3 tt0060548 silicon based life 
#>  4 tt0060548 medical ethics     
#>  5 tt0060548 pathologist        
#>  6 tt0060548 isolated community 
#>  7 tt0060548 cancer cure        
#>  8 tt0060548 science runs amok  
#>  9 tt0060548 radioactivity      
#> 10 tt0060548 medical research   
#> # ... with 3,993,868 more rows

read_tsv("title.languages.tsv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   tconst = col_character(),
#>   countries = col_character(),
#>   languages = col_character()
#> )
#> # A tibble: 418,228 x 3
#>    tconst     countries  languages
#>    <chr>      <chr>      <chr>    
#>  1 tt0007607  France     French   
#>  2 tt0345198  Turkey     Turkish  
#>  3 tt0014071  USA        <NA>     
#>  4 tt11405048 <NA>       <NA>     
#>  5 tt0053871  Sweden     Swedish  
#>  6 tt11179096 Japan      Japanese 
#>  7 tt0192355  France     French   
#>  8 tt0406830  UK,Germany <NA>     
#>  9 tt0099450  UK,USA     English  
#> 10 tt0049571  USA        English  
#> # ... with 418,218 more rows

read_tsv("title.releaseinfo.tsv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   tconst = col_character(),
#>   country = col_character(),
#>   data = col_character(),
#>   addition = col_character()
#> )
#> # A tibble: 1,159,195 x 4
#>    tconst    country   data             addition                       
#>    <chr>     <chr>     <chr>            <chr>                          
#>  1 tt1043471 Indonesia 1979             <NA>                           
#>  2 tt0034779 USA       16 January 1942  <NA>                           
#>  3 tt0034779 Mexico    27 October 1942  <NA>                           
#>  4 tt0034779 Sweden    19 February 1943 <NA>                           
#>  5 tt0034779 Portugal  23 October 1944  <NA>                           
#>  6 tt0959477 Japan     1 September 1973 <NA>                           
#>  7 tt2165975 Poland    20 November 2011 (Sputnik Russian Film Festival)
#>  8 tt7795706 France    2001             <NA>                           
#>  9 tt1928333 USA       10 July 2011     (Los Angeles                   
#> 10 tt0081780 Hong Kong 7 August 1980    <NA>                           
#> # ... with 1,159,185 more rows

read_tsv("title.technical.tsv", col_types = NULL)
#> Parsed with column specification:
#> cols(
#>   tconst = col_character(),
#>   aspectRatio = col_character(),
#>   filmLength = col_character(),
#>   negativeFormat = col_character(),
#>   cinematographicProcess = col_character(),
#>   printedFilmFormat = col_character(),
#>   camera = col_character()
#> )
#> # A tibble: 418,229 x 7
#>    tconst   aspectRatio filmLength   negativeFormat cinematographicPr~ printedFilmForm~ camera      
#>    <chr>    <chr>       <chr>        <chr>          <chr>              <chr>            <chr>       
#>  1 tt84422~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#>  2 tt00104~ 1.33 : 1    (5 reels) (~ 35 mm          Spherical          35 mm            <NA>        
#>  3 tt00390~ 1.37 : 1    <NA>         35 mm          Spherical          35 mm            <NA>        
#>  4 tt03532~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#>  5 tt84446~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#>  6 tt03345~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#>  7 tt13374~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#>  8 tt72277~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#>  9 tt31890~ 2.35 : 1    <NA>         <NA>           <NA>               D-Cinema         Red One Cam~
#> 10 tt01045~ <NA>        <NA>         <NA>           <NA>               <NA>             <NA>        
#> # ... with 418,219 more rows
```

## Tipps & Tricks

Eine Tabelle der in
[Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) indexierten
Slasherfilme (`Q853630`), die eine Referenz zur
[IMDb](https://www.imdb.com/) aufweisen, ist über folgende Abfrage zu
erhalten:

``` sql
SELECT ?item ?itemLabel ?imdb
WHERE {
  ?item wdt:P136 wd:Q853630 .
  ?item wdt:P345 ?imdb .
  
  SERVICE wikibase:label {
    bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en".
  }
}
```

## Weitere Informationen

`title.connections.tsv`, `title.keywords.tsv`, `title.languages.tsv`,
`title.releaseinfo.tsv` und `title.technical.tsv` enthalten
Informationen mit Stand vom 17.12.2019. Für alle weiteren (aktuellen)
Datensätze siehe: <https://www.imdb.com/interfaces/> und
<https://datasets.imdbws.com/>.
